import React, { useState, useEffect, useContext } from 'react'
import { StyleSheet, View, Text, ScrollView, Image, TouchableOpacity } from 'react-native';
import { ListItem } from '@rneui/themed';
import BottomSheetContext from './BottomSheetContext.js';
import PlayersContext from './PlayersContext.js';
import { GameEntryMore } from './GameEntryMore.js';
import { getProfilePicture } from './PlayersFunctions.js';


export default function GameEntry(props) {
    const [imageData1, setImageData1] = useState(null);
    const [imageData2, setImageData2] = useState(null);
    const { bottomSheetVisible, setBottomSheetVisible, setToDeleteId, setDeleteCallback, deleteCallback } = useContext(BottomSheetContext);
    const { playersData, setPlayersData } = useContext(PlayersContext);
    if (props.type === 1 && imageData1 === null) {
        const target1 = playersData.find((item) => item.id === props.id1);
        if (target1)
            setImageData1(target1.picture);
    }
    if (props.type === 1 && imageData2 === null) {
        const target2 = playersData.find((item) => item.id === props.id2);
        if (target2)
            setImageData2(target2.picture);
    }

    return (
        <View key={props.key}>
            <TouchableOpacity onPress={() => { props.viewFunc(props.id) }}
                onLongPress={() => {
                    setToDeleteId(props.id);
                    setBottomSheetVisible(!bottomSheetVisible);
                }}
            >
                <View style={styles.pending_entry}>
                    <View style={styles.pending_entry_image_container}>
                        {props.type === 1 && (
                            <Image source={{ uri: imageData1 }}
                                style={styles.pending_entry_image} />
                        )}
                        {props.type === 2 && (
                            <Image source={require('./assets/one.png')}
                                style={styles.pending_entry_image} />
                        )}
                        <Text>{props.type === 1 ? props.name1 : props.name1 + "/" + props.name2}</Text>
                        {props.winner > 0 && (
                            <Text style={(props.type === 1 ? props.id1 : 1) == props.winner ? styles.winnerText : styles.loserText}>{(props.type === 1 ? props.id1 : 1) == props.winner ? "Ganador" : "Perdedor"}</Text>
                        )}

                    </View>
                    <View style={styles.pending_entry_image_container}>
                        <Image source={require('./assets/competition.png')}
                            style={styles.pending_entry_image} />
                        <Text>Fecha: {props.date} </Text>
                    </View>
                    <View style={styles.pending_entry_image_container}>
                        {props.type === 1 && (
                            <Image source={{ uri: imageData2 }}
                                style={styles.pending_entry_image} />
                        )}
                        {props.type === 2 && (
                            <Image source={require('./assets/two.png')}
                                style={styles.pending_entry_image} />
                        )}
                        <Text>{props.type === 1 ? props.name2 : props.name3 + "/" + props.name4}</Text>
                        {props.winner > 0 && (
                            <Text style={(props.type === 1 ? props.id2 : 2) == props.winner ? styles.winnerText : styles.loserText}>{(props.type === 1 ? props.id2 : 2) == props.winner ? "Ganador" : "Perdedor"}</Text>
                        )}
                    </View>
                </View>
            </TouchableOpacity>
        </View>
    )
};

const styles = StyleSheet.create({
    pending_entry: {
        paddingTop: 50,
        paddingLeft: 30,
        paddingRight: 30,
        flexDirection: 'row',
    },
    pending_entry_image: {
        width: 100,
        height: 100,
        borderRadius: 100
    },
    pending_entry_image_container: {
        paddingRight: 20
    },
    pending_entry_options_button_container: {
        paddingLeft: 70
    },
    pending_entry_options_button_image: {
        width: 15,
        height: 50,
        borderRadius: 30
    },
    pending_entry_player_name: {
        paddingBottom: 15,
        fontWeight: 'bold'
    },
    winnerText: {
        color: 'green'
    },
    loserText: {
        color: 'red'
    },
    moreContainer: {
    },
    moreSave: {
        width: 50,
        height: 50
    },
});
