import { useState, useEffect, useContext } from 'react'
import { View, Text, TouchableOpacity, Button, ScrollView } from 'react-native';
import { ListItem, Card } from '@rneui/themed';
import DateTimePicker from '@react-native-community/datetimepicker';
import { addGame } from './GameFetchFunctions.js';
import { getCivs } from './CivsFunctions.js';
import { getResources } from './ResourcesFunctions.js';
import PlayersContext from './PlayersContext.js';
import CivsContext from './CivsContext.js';
import { CheckBox } from '@rneui/themed';
import PlayerForm from './PlayerForm.js';

export default function AddGameScreen(props) {
    const playerPlaceHolder = "Selecciona jugador";
    const datePlaceholder = "Selecciona fecha";
    const resourcesPlaceholder = "Selecciona recursos";
    // selected players 
    const [selectedPlayerId1, setSelectedPlayerId1] = useState(0);
    const [selectedPlayerId2, setSelectedPlayerId2] = useState(0);
    const [selectedPlayerId3, setSelectedPlayerId3] = useState(0);
    const [selectedPlayerId4, setSelectedPlayerId4] = useState(0);
    // selected civs
    const [selectedCivId1, setSelectedCivId1] = useState(0);
    const [selectedCivId2, setSelectedCivId2] = useState(0);
    const [selectedCivId3, setSelectedCivId3] = useState(0);
    const [selectedCivId4, setSelectedCivId4] = useState(0);
    // selected winner
    const [isExpandedWinner, setIsExpandedWinner] = useState(false);
    const [selectedWinner, setSelectedWinner] = useState(playerPlaceHolder);
    const [selectedWinnerId, setSelectedWinnerId] = useState(0);
    // resources
    const [isExpandedResources, setIsExpandedResources] = useState(false);
    const [selectedResources, setSelectedResources] = useState(resourcesPlaceholder);
    const [selectedResourcesId, setSelectedResourcesId] = useState(0);
    //date
    const [selectedDate, setSelectedDate] = useState(new Date());
    const [selectedDateString, setSelectedDateString] = useState(datePlaceholder);
    const [datePickerShow, setDatePickerShow] = useState(false);
    //type of game
    const [selectedType, setSelectedType] = useState(0);
    const [resourcesData, setResourcesData] = useState([]);
    //data
    const { playersData, setPlayersData } = useContext(PlayersContext)
    const [civsData, setCivsData] = useState([]);

    const setSelectedPlayerData = (_playerId, _civId, _playerNo) => {
        switch (_playerNo) {
            case 1:
                setSelectedPlayerId1(_playerId);
                setSelectedCivId1(_civId);
                break;
            case 2:
                setSelectedPlayerId2(_playerId);
                setSelectedCivId2(_civId);
                break;
            case 3:
                setSelectedPlayerId3(_playerId);
                setSelectedCivId3(_civId);
                break;
            case 4:
                setSelectedPlayerId4(_playerId);
                setSelectedCivId4(_civId);
                break;
            default:
                break;
        }
    }
    const alertError = (_errorNo) => {
        let errores = [
            "No se ha seleccionado jugador 1",
            "No se ha seleccionado jugador 2",
            "No se ha seleccionado jugador 3",
            "No se ha seleccionado jugador 4",
            "No se ha seleccionado una fecha",
            "No se han seleccionado tipo de recursos",
        ]
        alert(errores[_errorNo]);
    }
    const int_addGame = () => {
        if (selectedPlayerId1 == 0) {
            alertError(0);
            return;
        }
        if (selectedPlayerId2 == 0) {
            alertError(1);
            return;
        }
        if (selectedType == 1 && selectedPlayerId3 == 0) {
            alertError(2);
            return;
        }
        if (selectedType == 1 && selectedPlayerId2 == 0) {
            alertError(3);
            return;
        }
        if (selectedDateString === datePlaceholder) {
            alertError(4);
            return;
        }
        if (selectedResourcesId == 0) {
            alertError(5);
            return;
        }

        let myFormData = new FormData();
        myFormData.append("id1", selectedPlayerId1);
        myFormData.append("civ1", selectedCivId1);

        myFormData.append("id2", selectedPlayerId2);
        myFormData.append("civ2", selectedCivId2);

        myFormData.append("idWinner", selectedWinnerId);
        myFormData.append("type", selectedType + 1);
        myFormData.append("date", selectedDateString);
        myFormData.append("resources", selectedResourcesId);
        if (selectedType == 1) {
            myFormData.append("id3", selectedPlayerId3);
            myFormData.append("civ3", selectedCivId3);

            myFormData.append("id4", selectedPlayerId4);
            myFormData.append("civ4", selectedCivId4);
        }
        addGame(myFormData,
            () => {
                props.route.params.params.updateGamesData();
                try {
                    props.route.params.params.navigation.navigate('gamesScreen');
                } catch { }
            }
        );
    }
    const selectWinner = (playerId, playerName) => {
        setIsExpandedWinner(false);
        setSelectedWinner(playerName);
        setSelectedWinnerId(playerId);
    }
    const selectDate = (event, selectedDate) => {
        setSelectedDate(selectedDate);
        setDatePickerShow(false);
        let year = selectedDate.toLocaleString().substr(6, 4);
        let month = (selectedDate.getMonth() + 1).toString();
        let day = selectedDate.getDate().toString();
        setSelectedDateString(day + "/" + month + "/" + year);
    }

    const component1v1 = () => {
        return (
            <View>
                <Card>
                    <Card.Title>Jugador 1</Card.Title>
                    <Card.Divider />
                    <PlayerForm playerNo={1} callback={setSelectedPlayerData} />
                </Card>
                <Card>
                    <Card.Title>Jugador 2</Card.Title>
                    <Card.Divider />
                    <PlayerForm playerNo={2} callback={setSelectedPlayerData} />
                </Card>
            </View>
        )
    }

    const component2v2 = () => {
        return (
            <View>
                <Card>
                    <Card.Title>Equipo 1</Card.Title>
                    <Card.Divider />
                    <PlayerForm playerNo={1} callback={setSelectedPlayerData} />
                    <PlayerForm playerNo={2} callback={setSelectedPlayerData} />
                </Card>
                <Card>
                    <Card.Title>Equipo 2</Card.Title>
                    <Card.Divider />
                    <PlayerForm playerNo={3} callback={setSelectedPlayerData} />
                    <PlayerForm playerNo={4} callback={setSelectedPlayerData} />
                </Card>
            </View>
        )
    }

    useEffect(() => {
        getCivs(setCivsData);
        getResources(setResourcesData);
    }, [])

    return (
        <CivsContext.Provider value={{ civsData }}>
            <ScrollView>
                <View>
                    <View row align="center" spacing={4}>
                        <CheckBox
                            checked={selectedType === 0}
                            onPress={() => setSelectedType(0)}
                            checkedIcon="dot-circle-o"
                            uncheckedIcon="circle-o"
                            title="1vs1"
                        />
                        <CheckBox
                            checked={selectedType === 1}
                            onPress={() => setSelectedType(1)}
                            checkedIcon="dot-circle-o"
                            uncheckedIcon="circle-o"
                            title="2vs2"
                        />
                    </View>
                    {selectedType === 0 && (
                        component1v1()
                    )}
                    {selectedType === 1 && (
                        component2v2()
                    )}
                    <Card>
                        <Card.Title>Datos extra</Card.Title>
                        <Card.Divider />
                        <View>
                            <TouchableOpacity onPress={() => { setDatePickerShow(true) }} >
                                <Text>Fecha:</Text>
                                <Text>{selectedDateString}</Text>
                            </TouchableOpacity>
                            {datePickerShow && (
                                <DateTimePicker
                                    value={selectedDate}
                                    mode='date'
                                    onChange={selectDate}
                                />
                            )}
                        </View>
                        <View>
                            <Text>Ganador</Text>
                            <ListItem.Accordion
                                isExpanded={isExpandedWinner}
                                onPress={() => { setIsExpandedWinner(!isExpandedWinner); }}
                                content={
                                    <ListItem.Content>
                                        <ListItem.Title>{selectedWinner}</ListItem.Title>
                                    </ListItem.Content>
                                }
                            >
                                {selectedType === 0 && playersData.map(item => (
                                    <TouchableOpacity onPress={() => { selectWinner(item.id, item.name, 3) }} key={"top3" + item.id}>
                                        <ListItem >
                                            <ListItem.Content>
                                                <ListItem.Title>{item.name}</ListItem.Title>
                                            </ListItem.Content>
                                        </ListItem>
                                    </TouchableOpacity>
                                ))}
                                {selectedType === 1 && (
                                    <View>
                                        <TouchableOpacity onPress={() => { selectWinner(1, "Equipo 1", 3) }}>
                                            <ListItem >
                                                <ListItem.Content>
                                                    <ListItem.Title>Equipo 1</ListItem.Title>
                                                </ListItem.Content>
                                            </ListItem>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => { selectWinner(2, "Equipo 2", 3) }}>
                                            <ListItem >
                                                <ListItem.Content>
                                                    <ListItem.Title>Equipo 2</ListItem.Title>
                                                </ListItem.Content>
                                            </ListItem>
                                        </TouchableOpacity>
                                    </View>
                                )}
                            </ListItem.Accordion>
                        </View>
                        <View>
                            <Text>Recursos</Text>
                            <ListItem.Accordion
                                isExpanded={isExpandedResources}
                                onPress={() => { setIsExpandedResources(!isExpandedResources); }}
                                content={
                                    <ListItem.Content>
                                        <ListItem.Title>{selectedResources}</ListItem.Title>
                                    </ListItem.Content>
                                }
                            >
                                {resourcesData.map(item => (
                                    <TouchableOpacity onPress={() => {
                                        setSelectedResourcesId(item.id);
                                        setSelectedResources(item.resource);
                                        setIsExpandedResources(false);
                                    }}
                                        key={"top3" + item.id}>
                                        <ListItem >
                                            <ListItem.Content>
                                                <ListItem.Title>{item.resource}</ListItem.Title>
                                            </ListItem.Content>
                                        </ListItem>
                                    </TouchableOpacity>
                                ))}
                            </ListItem.Accordion>
                        </View>
                    </Card>
                    <Button title='Guardar Juego' onPress={int_addGame} />
                </View>
            </ScrollView>
        </ CivsContext.Provider>
    )
}
