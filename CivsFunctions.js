import ApiUrl from './ApiUrl.js';

export function getCivs(callback) {
    fetch(ApiUrl + "civs").then(response => response.json())
        .then(json => {
            callback(json);
        })
        .catch(error => {
            console.error("Error getting all civs", error);
        })
}
