import { useState, useContext, useEffect } from 'react'
import { View, ScrollView } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import Title from './Title.js';
import GamesContext from './GamesContext.js';
import { getGames } from './GameFetchFunctions.js';
import GameEntry from './GameEntry.js';
import TournementDropdown from './TournementDropdown.js';
import { getLeaderboard } from './PlayersFunctions.js';
import { getCurrentTournement } from './TournementFunctions.js';

const Tab = createMaterialTopTabNavigator();

export default function GamesScreen({ navigation }) {
    const [selectedTournement, setSelectedTournement] = useState(0);
    const [selectedTournementName, setSelectedTournementName] = useState("");
    const [pendingData, setPendingData] = useState([]);
    const [historyData, setHistoryData] = useState([]);
    const updateGamesData = (tournement) => {
        if (tournement == undefined) {
            tournement = selectedTournement;
        }
        getGames(1, tournement, setPendingData);
        getGames(2, tournement, setHistoryData);
    }
    const goAddGame = ({ navigation }) => {
        const params = { navigation: navigation, updateGamesData: updateGamesData };
        navigation.navigate('addGameScreen', { params });
    };
    const goViewGame = (idGame) => {
        console.log(idGame);
        navigation.navigate('singleGameScreen', { idGame: idGame });
    };
    const createGameScreen = (type) => {
        let tmpGameData = type == 1 ? pendingData : historyData;
        return (
            <View>
                <ScrollView >
                    {tmpGameData.map(item => (
                        GameEntry({ key: item.id, id: item.id, id1: item.id1, id2: item.id2, id3: item.id3, id3: item.id3, type: item.type, date: item.date, winner: item.winner, name1: item.name1, name2: item.name2, name3: item.name3, name4: item.name4, updateFunc: () => { }, viewFunc: goViewGame })
                    ))}
                </ScrollView>
            </View>
        )
    }

    useEffect(() => {
        const tournementPromise = [new Promise((resolve) => {
            getCurrentTournement((json) => {
                setSelectedTournement(json[0].id);
                setSelectedTournementName(json[0].name);
                resolve(json[0].id);
            })
        })];
        Promise.all(tournementPromise)
            .then((results) => {
                updateGamesData(results[0]);
            })
    }, []);

    const PendingScreen = () => { return (createGameScreen(1)) };
    const HistoryScreen = () => { return (createGameScreen(2)) };
    const dropdownComponent = TournementDropdown(selectedTournement, selectedTournementName, updateGamesData);

    return (
        <NavigationContainer independent={true} >
            <Title title='Juegos' addButton={true} callback={() => { goAddGame({ navigation }); }} />
            {dropdownComponent}
            <Tab.Navigator  >
                <Tab.Screen name="Pendientes" component={PendingScreen} />
                <Tab.Screen name="Historial" component={HistoryScreen} />
            </Tab.Navigator>
        </NavigationContainer>
    );
};
