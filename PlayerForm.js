import { useContext, useState } from 'react'
import { View, Text, TouchableOpacity, ScrollView, Modal, Button } from 'react-native';
import { ListItem } from '@rneui/themed';
import PlayersContext from './PlayersContext.js';
import CivsContext from './CivsContext.js';

const PlayerForm = ({ playerNo, callback }) => {
  const playerPlaceHolder = "Selecciona jugador";
  const civPlaceHolder = "Selecciona civilizacion";
  const { playersData, setPlayersData } = useContext(PlayersContext)
  const [isExpandedPlayer, setIsExpandedPlayer] = useState(false);
  const [selectedPlayer, setSelectedPlayer] = useState(playerPlaceHolder);
  const [selectedPlayerId, setSelectedPlayerId] = useState(0);
  const [isExpandedCiv, setIsExpandedCiv] = useState(false);
  const [selectedCiv, setSelectedCiv] = useState(civPlaceHolder);
  const [selectedCivId, setSelectedCivId] = useState(0);
  const { civsData, setCivsData } = useContext(CivsContext)

  const selectPlayer = (playerId, playerName) => {
    setIsExpandedPlayer(false);
    setSelectedPlayer(playerName);
    setSelectedPlayerId(playerId);
    callback(playerId, selectedCivId, playerNo);
  }
  const selectCiv = (civId, civName) => {
    setIsExpandedCiv(false);
    setSelectedCiv(civName);
    setSelectedCivId(civId);
    callback(selectedPlayerId, civId, playerNo);
  }
  return (
    <View>
      <Text>Jugador {playerNo}</Text>
      <ListItem.Accordion
        isExpanded={isExpandedPlayer}
        onPress={() => { setIsExpandedPlayer(!isExpandedPlayer); }}
        content={
          <ListItem.Content>
            <ListItem.Title>{selectedPlayer}</ListItem.Title>
          </ListItem.Content>
        }
      >
        {playersData.map(item => (
          <TouchableOpacity onPress={() => { selectPlayer(item.id, item.name, 1) }} key={"top1" + item.id}>
            <ListItem >
              <ListItem.Content>
                <ListItem.Title>{item.name}</ListItem.Title>
              </ListItem.Content>
            </ListItem>
          </TouchableOpacity>
        ))}
      </ListItem.Accordion>
      <Text>Civilizacion</Text>
      <ListItem.Accordion
        isExpanded={true}
        onPress={() => { setIsExpandedCiv(true); }}
        content={
          <ListItem.Content>
            <ListItem.Title>{selectedCiv}</ListItem.Title>
          </ListItem.Content>
        }
      >
      </ListItem.Accordion>
      <Modal visible={isExpandedCiv}>
        <ScrollView>
          <TouchableOpacity onPress={() => { selectCiv(0, "Calculo Balance", 1) }} key={"top1balance"}>
            <ListItem >
              <ListItem.Content>
                <ListItem.Title>Calculo de Balance</ListItem.Title>
              </ListItem.Content>
            </ListItem>
          </TouchableOpacity>
          {civsData.map(item => (
            <TouchableOpacity onPress={() => { selectCiv(item.id, item.name, 1) }} key={"top1" + item.id}>
              <ListItem >
                <ListItem.Content>
                  <ListItem.Title>{item.name}</ListItem.Title>
                </ListItem.Content>
              </ListItem>
            </TouchableOpacity>
          ))}
        </ScrollView>
      </Modal>
    </View>
  )
}

export default PlayerForm;
