import { ScrollView, View } from 'react-native';
import { useState, useEffect } from 'react'
import { getGame } from './GameFetchFunctions.js';
import { Card, Text } from '@rneui/themed';

const Game = (props) => {
    const [gameData, setGameData] = useState([]);

    useEffect(() => {
        getGame(props.route.params.idGame, setGameData);
    }, []);

    return (
        <View>
            {gameData != undefined && gameData.length > 0 && (
                <ScrollView>
                    <Card>
                        <Card.Title>Resumen de juego</Card.Title>
                        <Card.Divider />
                        <Text h4>Fecha</Text>
                        <Text>{gameData[0].date}</Text>
                        <Text h4>Ganador</Text>
                        <Text>{gameData[0].winner > 0 ? "Equipo " + gameData[0].winner : "Pendiente de jugar"}</Text>
                        <Text h4>Recursos</Text>
                        <Text>{gameData[0].resources}</Text>
                        <Text h4>Tipo</Text>
                        <Text>{gameData[0].gameType === 1 ? "1vs1" : "2vs2"}</Text>
                    </Card>
                    <Card>
                        <Card.Title>Equipo 1 </Card.Title>
                        <Card.Divider />
                        <Text h4>Jugador 1</Text>
                        <Text>{gameData[0].name1}</Text>
                        <Text h4>Civilizacion</Text>
                        <Text>{gameData[0].civ1}</Text>
                        {gameData[0].gameType === 2 && (
                            <View>
                                <Text h4>Jugador 2</Text>
                                <Text>{gameData[0].name2}</Text>
                                <Text h4>Civilizacion</Text>
                                <Text>{gameData[0].civ2}</Text>
                            </View>
                        )}
                    </Card>
                    <Card>
                        <Card.Title>Equipo 2</Card.Title>
                        <Card.Divider />
                        {gameData[0].gameType === 1 && (
                            <View>
                                <Text h4>Jugador 2</Text>
                                <Text>{gameData[0].name2}</Text>
                                <Text h4>Civilizacion</Text>
                                <Text>{gameData[0].civ2}</Text>
                            </View>
                        )}
                        {gameData[0].gameType === 2 && (
                            <View>
                                <Text h4>Jugador 3</Text>
                                <Text>{gameData[0].name3}</Text>
                                <Text h4>Civilizacion</Text>
                                <Text>{gameData[0].civ3}</Text>
                                <Text h4>Jugador 4</Text>
                                <Text>{gameData[0].name4}</Text>
                                <Text h4>Civilizacion</Text>
                                <Text>{gameData[0].civ4}</Text>
                            </View>
                        )}
                    </Card>
                </ScrollView>
            )}
        </View>
    )
}

export default Game;