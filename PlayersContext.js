import { createContext, useContext } from 'react';

const PlayersContext = createContext({});

export default PlayersContext;
