import React, { useState, createContext, useContext } from 'react';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, ScrollView, View, Image, TouchableOpacity } from 'react-native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Title from "./Title.js";
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { BottomSheet, ListItem } from '@rneui/themed';
import BottomSheetContext from './BottomSheetContext.js';
import { deleteGame } from './GameFetchFunctions.js';
import Players from './Players.js';
import MainNavigation from './MainNavigation.js';

const Stack = createNativeStackNavigator();

export default function App() {
    const [bottomSheetVisible, setBottomSheetVisible] = useState(false);
    const [toDeleteId, setToDeleteId] = useState(0);
    const [deleteCallback, setDeleteCallback] = useState(() => { });
    return (
        <BottomSheetContext.Provider value={{ bottomSheetVisible, setBottomSheetVisible, setToDeleteId, setDeleteCallback, deleteCallback }} >
            <SafeAreaProvider>
                <Players>
                    <MainNavigation>
                    </MainNavigation>
                </Players>
                <BottomSheet modalProps={{}} isVisible={bottomSheetVisible}>
                    <TouchableOpacity onPress={() => { deleteGame(toDeleteId,); }} >
                        <ListItem containerStyle={styles.deleteButton}>
                            <ListItem.Content>
                                <ListItem.Title style={{ color: 'white' }}>Eliminar</ListItem.Title>
                            </ListItem.Content>
                        </ListItem>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => { setBottomSheetVisible(false); }} >
                        <ListItem >
                            <ListItem.Content>
                                <ListItem.Title >Cancelar</ListItem.Title>
                            </ListItem.Content>
                        </ListItem>
                    </TouchableOpacity>
                </BottomSheet>
            </SafeAreaProvider>
        </BottomSheetContext.Provider >
    )
}

const styles = StyleSheet.create({
    main_content: {
        paddingBottom: 100
    },
    deleteButton: {
        backgroundColor: 'red',
        fontColor: 'white'
    }
});
