import ApiUrl from './ApiUrl.js';
export function getCurrentTournement(callback) {
    fetch(ApiUrl + "currenttournement").then(response => response.json())
        .then(json => {
            callback(json);
        })
        .catch(error => {
            console.error("Error getting current tournement", error);
        })
}

export function getAllTournements(callback) {
    fetch(ApiUrl + "tournements").then(response => response.json())
        .then(json => {
            callback(json);
        })
        .catch(error => {
            console.error("Error getting all tournement", error);
        })
}
