import React, { useState, useEffect, useContext } from 'react'
import { StyleSheet, View, ScrollView, TouchableOpacity } from 'react-native';
import Title from './Title.js';
import LeaderboardEntry from './leaderboard_entry.js';
import PlayersContext from './PlayersContext.js';
import TournementDropdown from './TournementDropdown.js';
import { getLeaderboard } from './PlayersFunctions.js';
import { getCurrentTournement } from './TournementFunctions.js';

const Leaderboard = ({ navigation }) => {
  const { playersData, setPlayersData, playersPictures } = useContext(PlayersContext);
  const [selectedTournement, setSelectedTournement] = useState(0);
  const [selectedTournementName, setSelectedTournementName] = useState("");
  const [leaderboardData, setLeaderboardData] = useState([]);

  const updateLeaderboard = (tournement) => {
    getLeaderboard(tournement, setLeaderboardData);
    setSelectedTournement(tournement);
  }

  useEffect(() => {
    const tournementPromise = [new Promise((resolve) => {
      getCurrentTournement((json) => {
        setSelectedTournement(json[0].id);
        setSelectedTournementName(json[0].name);
        resolve(json[0].name);
      })
    })];
    Promise.all(tournementPromise)
      .then((results) => {
        getLeaderboard(results[0], setLeaderboardData);
      })
  }, [])
  const dropdownComponent = TournementDropdown(selectedTournement, selectedTournementName, updateLeaderboard);

  return (
    <View>
      <Title title='Tabla de puntuaciones' />
      {dropdownComponent}
      <ScrollView>
        {leaderboardData.map(item => (
          <LeaderboardEntry
            key={item.id}
            id={item.id}
            player_name={item.name}
            played={item.played}
            won={item.wins}
            lost={item.loses}
          />
        ))}
      </ScrollView>
    </View>
  )
}

export default Leaderboard;

