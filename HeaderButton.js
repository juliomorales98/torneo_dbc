import { StyleSheet, TouchableOpacity, Text } from 'react-native';

const HeaderButton = (props) => {
    return (
        <TouchableOpacity onPress={() => { props.callback(); }}>
            <Text style={styles.blueText}> {props.text}</Text>
        </TouchableOpacity>
    )
}

export default HeaderButton;

const styles = StyleSheet.create({
    blueText: {
        color: 'blue'
    }
})
