import { StyleSheet, Image } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import Leaderboard from "./Leaderboard.js";
import Games from "./Games.js";

const Tab = createMaterialTopTabNavigator();
const GamesComponent = () => (Games());
export default function MainNavigation() {
    return (
        <NavigationContainer>
            <Tab.Navigator tabBarPosition='bottom'>
                <Tab.Screen
                    name="leaderboard" component={Leaderboard}
                    options={{
                        title: '',
                        tabBarIcon: ({ focused }) => (
                            <Image
                                source={require('./assets/podium.png')}
                                style={styles.menu_image}
                            />
                        ),
                        swipeEnabled: false
                    }}
                />
                <Tab.Screen
                    name="games" component={GamesComponent}
                    options={{
                        title: '',
                        tabBarIcon: ({ focused }) => (
                            <Image
                                source={require('./assets/schedule.png')}
                                style={styles.menu_image}
                            />
                        ),
                        swipeEnabled: false,
                    }}
                />
            </Tab.Navigator>
        </NavigationContainer>
    );
}

const styles = StyleSheet.create({
    menu_image: {
        width: 30,
        height: 30,
    },
});
