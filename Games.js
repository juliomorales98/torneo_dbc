import React, { useState, useEffect } from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Title from './Title.js';
import GameEntry from './GameEntry.js';
import HeaderButton from './HeaderButton.js';
import GamesContext from './GamesContext.js';
import GamesScreen from './GamesScreen.js';
import AddGameScreen from './AddGameScreen.js';
import { getGames } from './GameFetchFunctions.js';
import Game from './Game.js';

export default function Games() {
    const Stack = createNativeStackNavigator();
    return (
        <NavigationContainer independent={true}>
            <Stack.Navigator initialRouteName='gamesScreen'>
                <Stack.Screen name='gamesScreen' component={GamesScreen} options={{ headerShown: false }} />
                <Stack.Screen name='addGameScreen' component={AddGameScreen}
                    options={{
                        title: 'Agregar juego',
                    }}
                />
                <Stack.Screen name='singleGameScreen' component={Game}
                    options={{
                        title: 'Informacion de juego'
                    }} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}
