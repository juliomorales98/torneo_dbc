import { View, Text, TouchableOpacity, Image, StyleSheet } from 'react-native';

const Title = props => {
    return (
        <View style={styles.titleContainer}>
            <Text style={styles.titleText}>{props.title}</Text>
            {props.addButton && (
                <TouchableOpacity onPress={props.callback}>
                    <Image source={require('./assets/plus.png')} style={styles.titleImage} />
                </TouchableOpacity>
            )}
        </View>
    )
}
export default Title

const styles = StyleSheet.create({
    titleContainer: {
        flexDirection: 'row',
        backgroundColor: 'white',
        justifyContent: 'space-between',
        paddingTop: 30,
        paddingLeft: 10,
        paddingRight: 10,
    },
    titleText: {
        fontSize: 30,
    },
    titleImage: {
        width: 30,
        height: 30,
    }
})
