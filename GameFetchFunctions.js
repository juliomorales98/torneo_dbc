import ApiUrl from './ApiUrl.js';

export function deleteGame(gameId, callback) {
    let myFormData = new FormData();
    myFormData.append("gameId", gameId);
    fetch(ApiUrl + "deletegame", { method: "POST", headers: { 'Content-Type': 'multipart/form-data' }, body: myFormData })
        .then(response => {
            if (response.ok) {
                alert("Juego eliminado");
                //callback() ;
            }
            else {
                throw new Error();
            }
        })
        .catch(error => {
            alert("No se pudo eliminar informacion de juego. Intentelo mas tarde.");
            console.error("Error trying to delete game", error);
        })
};

export function getGames(type, tournementId, callback) {
    // types -> 0 all, 1 pending, 2 history
    let apiTypes = ["game", "pending", "history"];
    let myFormData = new FormData();
    myFormData.append("tournementId", tournementId);
    fetch(ApiUrl + apiTypes[type], { method: "POST", headers: { 'Content-Type': 'multipart/form-data' }, body: myFormData })
        .then(response => response.json())
        .then(json => {
            callback(json);
        }).catch(error => {
            console.error("Error retriving data");
            console.error(error);
        })
};

export function addGame(_formData, callback) {

    fetch(ApiUrl + "newgame", { method: "POST", headers: { 'Content-Type': 'multipart/form-data' }, body: _formData })
        .then(response => {
            if (response.ok) {
                callback();
            } else {
                console.error("Error inserting data into games table");
            }
        }).catch(error => {
            console.error("Error inserting data into games table");
            console.error(error);
        })
}

export function setGameWinner(gameId, winnerId, callback) {
    let myFormData = new FormData();
    myFormData.append("gameId", gameId);
    myFormData.append("winnerId", winnerId);
    fetch(ApiUrl + "setgamewinner", { method: "POST", headers: { 'Content-Type': 'multipart/form-data' }, body: myFormData })
        .then(response => {
            if (response.ok)
                callback();
            else
                throw new Error();
        })
        .catch(error => {
            alert("No se pudo actualizar informacion de juego. Intentelo mas tarde.");
            console.log(error);
            callback();
        })
}

export function getGame(idGame, callback) {
    // types -> 0 all, 1 pending, 2 history
    let myFormData = new FormData();
    myFormData.append("idGame", idGame);
    fetch(ApiUrl + "/game/" + idGame, { method: "GET" })
        .then(response => response.json())
        .then(json => {
            callback(json);
        }).catch(error => {
            console.error("Error retriving single game data");
            console.error(error);
        })
};