import { setGameWinner } from './GameFetchFunctions.js';
export default function GameEntryMore({ id1, name1, id2, name2, idGame, callback }) {
  const [isExpanded, setIsExpanded] = useState(false);
  const [selectedPlayer, setSelectedPlayer] = useState("Jugador ganador");

  const initializeDropdown = () => {
    /*setSelectedPlayer("Jugador ganador");
    setIsExpanded(false ) ;
    setIsSelected(false) ;*/
    callback('Pendientes');
  }

  const selectWinner = (winnerId, winnerName) => {
    setIsExpanded(false);
    setSelectedPlayer(winnerName);
    setGameWinner(idGame, winnerId, callback);
  }

  return (
    <View style={styles.moreContainer}>
      <ListItem.Accordion
        isExpanded={isExpanded}
        onPress={() => { setIsExpanded(!isExpanded); }}
        content={
          <ListItem.Content>
            <ListItem.Title>{selectedPlayer}</ListItem.Title>
          </ListItem.Content>
        }
      >
        <TouchableOpacity onPress={() => { selectWinner(id1, name1); }}>
          <ListItem >
            <ListItem.Content>
              <ListItem.Title>{name1}</ListItem.Title>
            </ListItem.Content>
          </ListItem>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => { selectWinner(id2, name2); }}>
          <ListItem >
            <ListItem.Content>
              <ListItem.Title>{name2}</ListItem.Title>
            </ListItem.Content>
          </ListItem>
        </TouchableOpacity>
      </ListItem.Accordion>
    </View>
  )
}
