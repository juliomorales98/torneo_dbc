import React, { useState, useEffect, useContext } from 'react'
import { StyleSheet, View, Text, TouchableOpacity, Image } from 'react-native'
import PlayersContext from './PlayersContext.js';
import { getProfilePicture } from './PlayersFunctions.js';

const LeaderboardEntry = props => {
    const [imageData, setImageData] = useState(null);
    const { playersData, setPlayersData } = useContext(PlayersContext);

    if (imageData == null) {
        const target = playersData.find((item) => item.id === props.id);
        if (target) {
            setImageData(target.picture);
        }
    }
    return (
        <View style={styles.leaderboard_entry}>
            <View style={styles.leaderboard_entry_image_container}>
                <Image source={{ uri: imageData }}
                    style={styles.leaderboard_entry_image} />
            </View>
            <View>
                <Text style={styles.leaderboard_entry_player_name}>Jugador: {props.player_name}</Text>
                <Text>Partidas jugadas: {props.played}</Text>
                <Text>Partidas ganadas: {props.won} </Text>
                <Text>Partidas perdidas: {props.lost} </Text>
            </View>
            <View style={styles.leaderboard_entry_options_button_container}>
                <TouchableOpacity >
                    <Image
                        source={require('./assets/more.png')}
                        style={styles.leaderboard_entry_options_button_image}
                    />
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    leaderboard_entry: {
        paddingLeft: 30,
        paddingRight: 30,
        margin: 10,
        flexDirection: 'row',
    },
    leaderboard_entry_image: {
        width: 100,
        height: 100,
        borderRadius: 100
    },
    leaderboard_entry_image_container: {
        paddingRight: 35
    },
    leaderboard_entry_options_button_container: {
        paddingLeft: 70
    },
    leaderboard_entry_options_button_image: {
        width: 15,
        height: 50,
        borderRadius: 30
    },
    leaderboard_entry_player_name: {
        paddingBottom: 15,
        fontWeight: 'bold'
    },

});
export default LeaderboardEntry 
