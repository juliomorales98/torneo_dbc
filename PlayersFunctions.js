import ApiUrl from './ApiUrl.js';

export function getLeaderboard(selectedTournement, callback) {
    const myFormData = new FormData();
    myFormData.append("tournementId", selectedTournement);
    fetch(ApiUrl + "leaderboard", { method: "POST", headers: { 'Content-Type': 'multipart/form-data' }, body: myFormData })
        .then(response => response.json())
        .then(json => {
            callback(json);
        }).catch(error => {
            console.error(error)
        })
}

export function getProfilePicture(playerid, callback) {
    const myFormData = new FormData();
    myFormData.append("id", playerid);
    fetch(ApiUrl + "profile_picture", { method: "POST", headers: { 'Content-Type': 'multipart/form-data' }, body: myFormData })
        .then(response => response.blob())
        .then(blob => {
            const fileReaderInstance = new FileReader();
            fileReaderInstance.readAsDataURL(blob);
            fileReaderInstance.onload = () => {
                base64data = fileReaderInstance.result;
                callback(base64data);
            }
        }).catch(error => {
            console.error(error)
        })
}

export function getAllProfilePictures(players) {
    const playersImage = []
    const requests = []
    players.map(item => {
        const myFormData = new FormData();
        myFormData.append("id", item.id);
        requests.push(fetch(ApiUrl + "profile_picture", { method: "POST", headers: { 'Content-Type': 'multipart/form-data' }, body: myFormData }).then((response) => response.blob()))
    })
    Promise.all(requests)
        .then((results) => {
            results.map(item => {
                const fileReaderInstance = new FileReader();
                fileReaderInstance.readAsDataURL(results);
                fileReaderInstance.onload = () => {
                    base64data = fileReaderInstance.result;
                    playersImage.push({ id: item.id, picture: base64data });
                    console.log("End fetch of", item.id, "at", new Date(), "vartype", typeof base64data);
                }
            })

        }).catch((error) => {
            console.error("Error fetching all profile pictures", error);
        })
    return playersImage;
}

export function getPlayers(callback) {
    fetch(ApiUrl + "players", { method: "GET" })
        .then(response => response.json())
        .then(json => {
            callback(json);
        }).catch(error => {
            console.error(error)
        })
}
