import ApiUrl from './ApiUrl.js';

export function getResources(callback) {
    fetch(ApiUrl + "resources").then(response => response.json())
        .then(json => {
            callback(json);
        })
        .catch(error => {
            console.error("Error getting all resources", error);
        })
}
