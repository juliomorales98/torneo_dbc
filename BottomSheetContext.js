import React, { createContext } from 'react';

const BottomSheetContext = createContext({});

export default BottomSheetContext;
