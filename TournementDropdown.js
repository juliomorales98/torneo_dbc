import { useState, useEffect } from 'react';
import { ListItem } from '@rneui/themed';
import { View, TouchableOpacity } from 'react-native';
import { getAllTournements } from './TournementFunctions.js';

const TournementDropdown = (_selectedTournementId, _selectedTournementName, callback) => {
    const [dropdownExpanded, setDropdownExpanded] = useState(false);
    const [tournements, setTournements] = useState([]);
    const [selectedTournementName, setSelectedTournementName] = useState(_selectedTournementName);
    const [selectedTournementId, setSelectedTournementId] = useState(_selectedTournementId);

    const selectTournement = (tournementId, tournementName) => {
        setDropdownExpanded(false);
        callback(tournementId);
        setSelectedTournementName(tournementName);
    }

    useEffect(() => {
        getAllTournements(setTournements);
    }, [])
    return (
        <View>
            <ListItem.Accordion
                isExpanded={dropdownExpanded}
                onPress={() => { setDropdownExpanded(!dropdownExpanded); }}
                content={
                    <ListItem.Content>
                        <ListItem.Title>{selectedTournementName}</ListItem.Title>
                    </ListItem.Content>
                }
            >
                {tournements.map(item => (
                    <TouchableOpacity onPress={() => { selectTournement(item.id, item.name) }} key={"top1" + item.id}>
                        <ListItem >
                            <ListItem.Content>
                                <ListItem.Title>{item.name}</ListItem.Title>
                            </ListItem.Content>
                        </ListItem>
                    </TouchableOpacity>
                ))}
            </ListItem.Accordion>
        </View>
    )
}

export default TournementDropdown;
