import React, { useState, useContext, useEffect } from 'react';
import PlayersContext from './PlayersContext.js';
import { getLeaderboard } from './PlayersFunctions.js';
import { getProfilePicture } from './PlayersFunctions.js';
import { getPlayers } from './PlayersFunctions.js';

export default function Players({ children }) {
    const [playersData, setPlayersData] = useState([]);
    const [playersPictures, setPlayersPictures] = useState([]);
    const callback = (json) => {
        const promises = json.map(item => {
            return new Promise((resolve) => {
                getProfilePicture(item.id, (image) => {
                    item.picture = image
                    resolve(item);
                });
            });
        });
        Promise.all(promises)
            .then((updatedItems) => {
                setPlayersData(updatedItems);
            }).catch((error) => {
                console.log("Error updating players info", error);
            });
    }

    useEffect(() => {
        getPlayers(callback);
    }, [])
    return (
        <PlayersContext.Provider value={{ playersData, setPlayersData, playersPictures, setPlayersPictures }}>
            {children}
        </PlayersContext.Provider>
    )
}
